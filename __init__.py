# -*- coding: utf-8 -*-

from trytond.pool import Pool
from . import configuration
from . import emergency


def register():
    Pool.register(
        configuration.HealthConfiguration,
        emergency.AdmissionCause,
        emergency.ImagingTestRequest,
        emergency.Emergency,
        emergency.VitalSigns,
        emergency.ToxicHabits,
        emergency.Interconsultation,
        emergency.HealthLabTest,
        emergency.Move,
        emergency.EmergencyProcedures,
        emergency.EmergencyMedication,
        emergency.Inpatient,
        emergency.NoteList,
        emergency.MedicTeam,
        emergency.ShipmentInternal,
        module='health_emergency', type_='model')
    Pool.register(
        emergency.MedicalReleaseReport,
        emergency.ReferredPatientReport,
        emergency.EvaluationsNotes,
        emergency.EmergencyMedicalHistory,
        module='health_emergency', type_='report')
    Pool.register(
        emergency.UndoDischarged,
        emergency.CancelDiagnosis,
        # emergency.CreateEmergencyShipment,
        module='health_emergency', type_='wizard')
