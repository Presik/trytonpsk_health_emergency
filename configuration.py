# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class HealthConfiguration(metaclass=PoolMeta):
    __name__ = 'health.configuration'
    emergency_sequence = fields.Many2One('ir.sequence', 'Emergency Sequence',
        required=True)
    location_emergency_default = fields.Many2One('stock.location',
        'Location Emergency Default', domain=[('type', '=', 'warehouse')])
    product_emergency = fields.Many2One('product.product', 'Product Emergency',
        domain=[('type', '=', 'service')])
    product_interconsultation = fields.Many2One('product.product',
        'Product Interconsultation', domain=[('type', '=', 'service')]
    )
    ward_emergency = fields.Many2One('health.hospital.ward', 'Emergency Ward')
    interconsultation_request_sequence = fields.Many2One(
        'ir.sequence', 'Interconsultation Sequence', required=True
    )
